package net.redbuffer.ptclsipprot;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import net.redbuffer.ptclsipprot.DatabaseHandler.CallLogItem;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CallLogAdapter extends ArrayAdapter<CallLogItem>
{
	private ArrayList<CallLogItem> objects;
	public CallLogAdapter(Context context,
			int textViewResourceId, ArrayList<CallLogItem> objects)
	{
		super(context, textViewResourceId, objects);
		this.objects = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if(v==null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.call_log_adapter, null);
		}
		CallLogItem log = objects.get(position);
		Log.d("debug",log.logId+"b");
		if(log!=null)
		{
			TextView logNumber = (TextView) v.findViewById(R.id.log_number);
			TextView logTime = (TextView) v.findViewById(R.id.log_time);
			TextView logDuration = (TextView) v.findViewById(R.id.log_duration);
			Log.d("testing", "here");

			Log.i("PTCLSIP:", log.logId + " : " + log.number + " : " + log.startTime + " : " + log.endTime + " : " + log.configId);
			logNumber.setText(log.number);
			Date d = new Date(log.startTime*1000);
			Log.i("PTCL TIME", d.toString());
			
			SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy,HH:mm");
			f.setTimeZone(TimeZone.getTimeZone("GMT"));
			String start_time = f.format(d); 
			logTime.setText(start_time);
			long duration = log.endTime - log.startTime;
			int [] splits = splitToComponentTimes(duration);
			logDuration.setText(Integer.toString(splits[1])+":"+Integer.toString(splits[2]));
		}
		return v;
	}
	
	public static int[] splitToComponentTimes(long biggy)
	{
	    long longVal = biggy;
	    int hours = (int) longVal / 3600;
	    int remainder = (int) longVal - hours * 3600;
	    int mins = remainder / 60;
	    remainder = remainder - mins * 60;
	    int secs = remainder;

	    int[] ints = {hours , mins , secs};
	    return ints;
	}
}