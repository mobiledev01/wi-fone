package net.redbuffer.ptclsipprot;

import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.LinphoneSimpleListener.LinphoneServiceListener;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCore.GlobalState;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.OnlineStatus;
import org.linphone.core.PresenceModel;
import org.linphone.mediastream.Log;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity implements ActionBar.TabListener,
					LinphoneServiceListener {
	public static final String PREF_FIRST_LAUNCH = "pref_first_launch";	
	public static MainActivity instance;
	
	private DatabaseHandler db;
	
	public ActionBar actionBar;
	
	TabsAdapter mPagerAdapter;
	ViewPager mViewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		instance = this;
		db = DatabaseHandler.getInstance(this);
		
		// Set up the action bar.
		actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		mPagerAdapter = new TabsAdapter(getSupportFragmentManager());
		
		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mPagerAdapter);
		mViewPager.setOffscreenPageLimit(4); 
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});
		
		for (int i = 0; i < mPagerAdapter.getCount(); i++) {
			actionBar.addTab(
					actionBar.newTab()
					.setText(mPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		
		if (!LinphoneManager.isInstanciated()) {
			Log.e("No service running: avoid crash by starting the launcher", this.getClass().getName());
			// super.onCreate called earlier
			finish();
			startActivity(getIntent().setClass(this, WaitingActivity.class));
			return;
		}
		
		boolean useFirstLoginActivity = getResources().getBoolean(R.bool.display_account_wizard_at_first_start);
		
		if (useFirstLoginActivity && LinphonePreferences.instance().isFirstLaunch()) {
			if (LinphonePreferences.instance().getAccountCount() > 0) {
				LinphonePreferences.instance().firstLaunchSuccessful();
			} else {
				startActivity(new Intent(this, RegistrationActivity.class));
			}
		} else {
			LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(0));
			LinphoneManager.getLc().setPresenceInfo(0, "", OnlineStatus.Online);
		}
		/*
		DatabaseHandler db = new DatabaseHandler(getBaseContext());
		db.createNewWiFoneContact("Umair", "Akhtar", "umair", "");
		db.createNewWiFoneContact("Hassaan", "Anjum", "hassaan", "");
		
		int id = 0;
		id = db.createNewConversation("umair");
		db.createNewMessage(id, 0, "hi", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 0, "how are you?", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 1, "hello", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 1, "I am fine.", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 0, "whats up?", (int)(System.currentTimeMillis()/1000), 0);
		
		id = db.createNewConversation("111");
		db.createNewMessage(id, 0, "hey, whats up?", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 0, "I was waiting for you.", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 1, "nothing interesting.", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 1, "anything interesting on your end?", (int)(System.currentTimeMillis()/1000), 0);
		db.createNewMessage(id, 0, "nope :(", (int)(System.currentTimeMillis()/1000), 0);
		*/
	}

	@Override
	protected void onStart() {
		super.onStart();
	}
	
	@Override
	protected void onResume() {
		super.onResume();		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		Intent i;
		int settingsId = 0;
		switch(item.getItemId())
		{
			case R.id.action_new_message:
				i = new Intent(this, NewMessageActivity.class);
				startActivity(i);
				return true;
			case R.id.action_add_contact:
				i = new Intent(this, Edit_WiFone_contact.class);
				i.putExtra("isAdd",true);
				startActivity(i);
				return true;
			case R.id.action_conversations:
				i = new Intent(this, ConversationListActivity.class);
				startActivity(i);
				return true;
			case R.id.action_app_to_app:
				i = new Intent(this,RegistrationActivity.class);
				i.putExtra("RegistrationActivity.settingsId", settingsId);
				startActivity(i);
				return true;
			case R.id.action_app_to_ptcl:
				i = new Intent(this, RegistrationActivity.class);
				settingsId = 1;
				i.putExtra("RegistrationActivity.settingsId", settingsId);
				startActivity(i);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		mViewPager.setCurrentItem(tab.getPosition());
		//Log.d("hello","test1203");
	}
	
	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) { }
	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) { }
	
	@Override
	protected void onStop() {
		Log.i("MainActivity: OnStop called.");
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
		Log.i("MainActivity: OnDestroy called.");
		instance = null;
		super.onDestroy();
	}

	@Override
	public void onGlobalStateChanged(GlobalState state, String message) { }
	@Override
	public void onCallStateChanged(LinphoneCall call, State state, String message) { }
	@Override
	public void onCallEncryptionChanged(LinphoneCall call, boolean encrypted, String authenticationToken) { }
	@Override
	public void tryingNewOutgoingCallButCannotGetCallParameters() { }
	@Override
	public void tryingNewOutgoingCallButWrongDestinationAddress() { }
	@Override
	public void tryingNewOutgoingCallButAlreadyInCall() { }
	@Override
	public void onDisplayStatus(String message) { }
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@SuppressLint("NewApi")
	@Override
	public void onRegistrationStateChanged(RegistrationState state, String message) {
		if (state == RegistrationState.RegistrationProgress) {
			Log.i("dispatched state [" + state + "]");
			actionBar.setIcon(R.drawable.ic_launcher_registering);
		} else if (state == RegistrationState.RegistrationNone) {
			Log.i("dispatched state [" + state + "]");
			actionBar.setIcon(R.drawable.ic_launcher_offline);
		} else if (state == RegistrationState.RegistrationCleared) {
			Log.i("dispatched state [" + state + "]");
			actionBar.setIcon(R.drawable.ic_launcher_offline);
		} else if (state == RegistrationState.RegistrationFailed) {
			Log.i("dispatched state [" + state + "]");
			actionBar.setIcon(R.drawable.ic_launcher_offline);
		} else if (state == RegistrationState.RegistrationOk) {
			Log.i("dispatched state [" + state + "]");
			actionBar.setIcon(R.drawable.ic_launcher);
		}
	}
}
