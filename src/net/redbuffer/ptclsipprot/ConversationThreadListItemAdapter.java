package net.redbuffer.ptclsipprot;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableRow;
import android.widget.TextView;

public class ConversationThreadListItemAdapter extends ArrayAdapter<DatabaseHandler.Message>{
	private ArrayList<DatabaseHandler.Message> objects;
	public ConversationThreadListItemAdapter(Context context, int textViewResourceId, ArrayList<DatabaseHandler.Message> objects)
	{
		super(context, textViewResourceId, objects);
		this.objects = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if(v==null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.conversation_thread_item, null);
		}
		
		DatabaseHandler.Message message = objects.get(position);
		if(message!=null)
		{
			Date d = new Date(message.timestamp*1000);
			SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy,HH:mm");
			f.setTimeZone(TimeZone.getTimeZone("GMT"));
			String message_time = f.format(d);
			
			if(message.direction == 0){
				TextView incoming_message = (TextView) v.findViewById(R.id.incoming_message);
				TextView incoming_message_time = (TextView) v.findViewById(R.id.incoming_message_time);
				incoming_message.setText(message.message);
				incoming_message_time.setText(message_time);
				
				TableRow incoming = (TableRow) v.findViewById(R.id.incoming_message_box);
				TableRow outgoing = (TableRow) v.findViewById(R.id.outgoing_message_box);
				incoming.setVisibility(TableRow.VISIBLE);
				outgoing.setVisibility(TableRow.GONE);
			}
			else if (message.direction == 1){
				TextView outgoing_message = (TextView) v.findViewById(R.id.outgoing_message);
				TextView outgoing_message_time = (TextView) v.findViewById(R.id.outgoing_message_time);
				outgoing_message.setText(message.message);
				outgoing_message_time.setText(message_time);
				
				TableRow incoming = (TableRow) v.findViewById(R.id.incoming_message_box);
				TableRow outgoing = (TableRow) v.findViewById(R.id.outgoing_message_box);
				incoming.setVisibility(TableRow.GONE);
				outgoing.setVisibility(TableRow.VISIBLE);
			}
		}
		return v;
	}
}
