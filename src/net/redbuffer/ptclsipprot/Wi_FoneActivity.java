package net.redbuffer.ptclsipprot;

import net.redbuffer.ptclsipprot.DatabaseHandler.Conversation;
import net.redbuffer.ptclsipprot.DatabaseHandler.WiFoneContact;

import org.linphone.LinphonePreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Wi_FoneActivity extends Activity {

    public String firstName = "";	
    public String lastName = "";
    public String userName = "";	
    public String domain = "";	
    public int contactId;
    public static String conversationId;
	public static String username;

	DatabaseHandler db;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifone_contact);
		
		db = DatabaseHandler.getInstance(this);
		
	    Bundle extras = getIntent().getExtras();
	    
		firstName = extras.getString("firstName");
		lastName = extras.getString("lastName");
		userName = extras.getString("userName");
		domain = extras.getString("domain");
		contactId = extras.getInt("contactId");
		
		TextView txtView_fn=(TextView) findViewById(R.id.wi_fone_firstname);
		txtView_fn.setText(firstName + " " + lastName);
		
		TextView txtView_un=(TextView) findViewById(R.id.wi_fone_username);
		txtView_un.setText(userName + "@" + domain);
		
		Button edit=(Button)findViewById(R.id.wi_fone_edit);
		
		edit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Intent intent = new Intent(v.getContext(), Edit_WiFone_contact.class);
            	intent.putExtra("firstName", firstName);
				intent.putExtra("lastName", lastName);
				intent.putExtra("userName", userName);
				intent.putExtra("domain", domain);
				intent.putExtra("contactId", contactId);
				intent.putExtra("isAdd", false);

				startActivity(intent);
            }
		});
		
		Button del = (Button) findViewById(R.id.wi_fone_del);
		
		del.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Log.d("here", "here: " + contactId);
            	int result = db.deleteWiFoneContact(contactId);
            	
            	if (result > 0) {
            		finish();
            	} else {
            		Toast.makeText(getApplicationContext(), "Error in deleting contact.", Toast.LENGTH_LONG).show();
            	}
            }
		});
		
		Button call = (Button) findViewById(R.id.wi_fone_call);
		
		call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               //call text here
            	Intent intent = new Intent(v.getContext(), CallingActivity.class);
				String sippAddress = "";
				intent.putExtra(sippAddress, userName);
				intent.putExtra("CallingActivity.configId", 0);
				startActivity(intent);
            }
		});
		
		Button Video_call = (Button) findViewById(R.id.wi_fone_video_call);
		Video_call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               //call text here
            	
            	Intent intent = new Intent(v.getContext(), CallingActivity.class);
				String sipAddress = "";
				intent.putExtra(sipAddress, userName);
				intent.putExtra("is_video_call", true);				
				intent.putExtra("CallingActivity.configId", 0);
				startActivity(intent);
            }
		});
		
		Button msg = (Button) findViewById(R.id.wi_fone_msg);
		
		msg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               //msg text here
            	conversationId = "";
				username = "";
				Conversation conv = db.findExistingConversation(userName);
				if (conv == null) {
					conv = db.new Conversation();
					conv.conversationId = db.createNewConversation(userName);
				}
				
				LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(0));
				
				Intent intent = new Intent(v.getContext(), ConversationActivity.class);
				intent.putExtra("ConversationActivity.conversationId", conv.conversationId);
				intent.putExtra(ConversationActivity.username, userName);
				startActivity(intent);
            }
       });
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try
		{
			WiFoneContact curr_cnt=db.getWiFoneContact(contactId);
			
			TextView txtView_fn=(TextView) findViewById(R.id.wi_fone_firstname);
			txtView_fn.setText(curr_cnt.firstName + " " + curr_cnt.lastName);
			
			TextView txtView_un=(TextView) findViewById(R.id.wi_fone_username);
			txtView_un.setText(curr_cnt.username + "@" + curr_cnt.domain);
		}catch(Exception e)
		{
			Log.d("error in wi_Fone activity",e.toString());
			exceptionHandler(e.toString());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add__edit, menu);
		return true;
	}
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(this, error, Toast.LENGTH_LONG);
		toast.show();
	}
}
