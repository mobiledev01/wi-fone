package net.redbuffer.ptclsipprot;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class dialerFragment extends Fragment {
	public final static String sipAddress = "";
	EditText numberDisplay;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.dialer_fragment,
				container, false);
		
		final Button button1 = (Button) view
				.findViewById(R.id.Button01);
		final Button button2 = (Button) view
				.findViewById(R.id.Button02);
		final Button button3 = (Button) view
				.findViewById(R.id.Button03);
		final Button button4 = (Button) view
				.findViewById(R.id.Button04);
		final Button button5 = (Button) view
				.findViewById(R.id.Button05);
		final Button button6 = (Button) view
				.findViewById(R.id.Button06);
		final Button button7 = (Button) view
				.findViewById(R.id.Button07);
		final Button button8 = (Button) view
				.findViewById(R.id.Button08);
		final Button button9 = (Button) view
				.findViewById(R.id.Button09);
		final Button button0 = (Button) view
				.findViewById(R.id.Button00);
		final Button buttonCall = (Button) view
				.findViewById(R.id.ButtonCall);
		final Button buttonDel = (Button) view
				.findViewById(R.id.ButtonDel);
		numberDisplay = (EditText) view
				.findViewById(R.id.numberDisplay);
		

		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "1");
			}
		});
		
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "2");
			}
		});
		
		button3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "3");
			}
		});
		
		button4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "4");
			}
		});
		
		button5.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "5");
			}
		});
		
		button6.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "6");
			}
		});
		
		button7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "7");
			}
		});
		
		button8.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "8");
			}
		});
		
		button9.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "9");
			}
		});
		
		button0.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				numberDisplay.setText(numberDisplay.getText()
						+ "0");
			}
		});
		
		buttonCall.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String phoneNumber = numberDisplay.getText().toString();
				if (phoneNumber != "") {
					Intent intent = new Intent(getActivity().getBaseContext(), CallingActivity.class);
					intent.putExtra(sipAddress, phoneNumber);
					intent.putExtra("CallingActivity.configId", 1);
					startActivity(intent);

				} else {
					Toast.makeText(getActivity(), "Invalid phone number.", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		buttonDel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(numberDisplay.getText().length() > 0){
					numberDisplay.getText().delete(numberDisplay.getText().length() - 1, numberDisplay.getText().length());
				}
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i("PTCL TEST SIP","RESUME CALLED");
		
		numberDisplay.setText("");

	}
	
}
