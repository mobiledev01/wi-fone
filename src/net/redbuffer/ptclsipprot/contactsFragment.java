package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class contactsFragment extends Fragment {
	ArrayList <String []> contacts = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		
		final View view = inflater.inflate(R.layout.contacts_fragment,container, false);
		
                
		new AsyncTask<Void, Void, Void>() 
        {
            @Override
            protected void onPreExecute() 
            {
            	
            }// End of onPreExecute method

            @Override
            protected Void doInBackground(Void... params) 
            {
        		contacts = getContacts();

                return null;
            }// End of doInBackground method

            @Override
            protected void onPostExecute(Void result)
            {
            	//ArrayList<String> contactNames = getContactNames();
        		final ListView contactsListView = (ListView) view.findViewById(R.id.contactsListView);
        		contactsListAdapter arrayAdapter = new contactsListAdapter(getActivity(), android.R.layout.simple_list_item_1, contacts);
        		//ArrayAdapter<String> arrayAdapter =  new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, contactNames);
        		contactsListView.setAdapter(arrayAdapter);
        		contactsListView.setOnItemClickListener(new OnItemClickListener() {
        			@Override
        			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        				String [] contact = contacts.get(position);
        				Intent intent = new Intent(getActivity().getBaseContext(), CallingActivity.class);
        				String phoneNumber = contact[2].toString();
        				if (phoneNumber != "") {
        					String sipAddress = "";
        					intent.putExtra(sipAddress, phoneNumber);
        					intent.putExtra("CallingActivity.configId", 1);
        					startActivity(intent);
        				} else {
        					Toast.makeText(getActivity(), "Invalid phone number.", Toast.LENGTH_LONG).show();
        				}
        			}
        		});

            }//End of onPostExecute method
        }.execute((Void[]) null);
    
		return view;
	
	}

	public ArrayList<String[]> getContacts() {
		ArrayList<String[]> contacts = new ArrayList<String[]>();
		try
		{
			ContentResolver cr = getActivity().getContentResolver();
			Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, "DISPLAY_NAME ASC");
	
			String phoneNo = "";
	
			if (cur.getCount() > 0) {
				while (cur.moveToNext()) {
	
					// get ID & Name of the contact
					String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
					String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	
					// get phone number
					Cursor pCur = cr.query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?",
							new String[] { id }, null);
					
					phoneNo = (pCur.moveToNext()) ? pCur
							.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
							: "Update";
					//Log.i("Phone Number :",phoneNo);
					//Log.i("Contact Name :",name);
					try {
						if(phoneNo!=""){
							contacts.add(new String[] { id, name, phoneNo });
						}
					} catch (Exception e) {
						Log.e("ERROR CURSOR: ", e.getMessage());
					}
					pCur.close();
				}
			}
		}catch(Exception e)
		{
			Log.d("error in contact fragments",e.toString());
			exceptionHandler(e.toString());
		}
		return contacts;
	}
	
	public ArrayList<String> getContactNames() {
		ArrayList<String> contacts = new ArrayList<String>();
		ContentResolver cr = getActivity().getContentResolver();
		Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, "DISPLAY_NAME ASC");

		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				try {
					contacts.add(name);
				} catch (Exception e) {
					Log.e("ERROR CURSOR: ", e.getMessage());
				}
			}
		}
		return contacts;
	}
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(getActivity(), error, Toast.LENGTH_LONG);
		toast.show();
	}
}
