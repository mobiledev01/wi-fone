package net.redbuffer.ptclsipprot;

import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;

import net.redbuffer.ptclsipprot.DatabaseHandler.Conversation;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NewMessageActivity extends Activity {

	DatabaseHandler db;
	public String conversationId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_message);
		
		db = DatabaseHandler.getInstance(getBaseContext());
		
		final Button btnSendMessage = (Button) findViewById(R.id.btnSendMessage);
		final EditText txtUsername = (EditText) findViewById(R.id.txtMsgUsername);
		final EditText txtMessage = (EditText) findViewById(R.id.txtMsgMessage);
		
		btnSendMessage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String username = txtUsername.getText().toString();
				String message = txtMessage.getText().toString();
				
				Conversation conv = db.findExistingConversation(username);
				if (conv == null) {
					conv = db.new Conversation();
					conv.conversationId = db.createNewConversation(username);
					conv.username = username;
					conv.lastActivityTime = (int)(System.currentTimeMillis()/1000);
				}
				
				MessageManager mgr = new MessageManager(getBaseContext());
				db.createNewMessage(conv.conversationId, 1, message, (int)(System.currentTimeMillis()/1000), 1);
				// set app-to-app server as default
				if (LinphoneManager.isInstanciated()) {
					LinphonePreferences prefs = LinphonePreferences.instance();
					String domain = prefs.getAccountDomain(prefs.getDefaultAccountIndex());
					mgr.sendTextMessage("sip:" + username + "@" + domain , message);
				}
				
				Intent intent = new Intent(getBaseContext(), ConversationActivity.class);
				intent.putExtra("ConversationActivity.conversationId", conv.conversationId);
				intent.putExtra(ConversationActivity.username, username);
				startActivity(intent);
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_message, menu);
		return true;
	}
}
