package net.redbuffer.ptclsipprot;

import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.LinphoneSimpleListener.LinphoneOnRegistrationStateChangedListener;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneCoreException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends Activity {
	
	public RegistrationActivity instance;
	private LinphonePreferences prefs;
	private DatabaseHandler db;
	private Handler mHandler = new Handler();
	private boolean accountCreated = false;
	public static int settingsId = 0;
	int index = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		//final databaseHandler db = new databaseHandler(this);
		
		instance = this;
		prefs = LinphonePreferences.instance();
		db = DatabaseHandler.getInstance(getBaseContext());
		
		if (prefs.isFirstLaunch()) {
			checkAccount("apptoapp", "password", "apptoapp.com");
			accountCreated = false;
			checkAccount("apptoptcl", "password", "apptoptcl.com");
			accountCreated = false;
			prefs.setDefaultAccount(0);
			prefs.firstLaunchSuccessful();
			
			db.setLinphoneAccountId(0, prefs.getDefaultAccountIndex());
			db.setLinphoneAccountId(1, prefs.getNonDefaultAccountIndex());
		}
		
		Intent intent = getIntent();
		settingsId = intent.getIntExtra("RegistrationActivity.settingsId", RegistrationActivity.settingsId);
		//settingsId = intent.getStringExtra(RegistrationActivity.settingsId);
		
		final EditText serverIP = (EditText) findViewById(R.id.server_ip_input);
		final EditText username = (EditText) findViewById(R.id.username_input);
		final EditText password = (EditText) findViewById(R.id.password_input);
		
		//HashMap<String, String> oldValues = db.getSettings();
		
		//serverIP.setText(oldValues.get("server_ip"));
		//serverProxy.setText(oldValues.get("server_proxy"));
		//username.setText(oldValues.get("username"));
		//password.setText(oldValues.get("password"));
		//description.setText(oldValues.get("description"));
		
		if (prefs.getAccountCount() > 0) {
			index = db.getLinphoneAccountId(settingsId);
			
			//index = prefs.getDefaultAccountIndex();
			serverIP.setText(prefs.getAccountDomain(index));
			username.setText(prefs.getAccountUsername(index));
			//serverProxy.setText(prefs.getAccountProxy(index));
			password.setText(prefs.getAccountPassword(index));
		}
		
		Button cancelButton = (Button) findViewById(R.id.cancel_button);
		Button updateButton = (Button) findViewById(R.id.update_button);
		
		updateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String un = username.getText().toString();
				String pw = password.getText().toString();
				String dm = serverIP.getText().toString();
				//String pr = serverProxy.getText().toString();
				int saved_index;
				if(settingsId==0)
				{
					saved_index = db.getLinphoneAccountId(1);
				}else
				{
					saved_index = db.getLinphoneAccountId(0);
				}
				String saved_un= prefs.getAccountUsername(saved_index);
				String saved_dm= prefs.getAccountDomain(saved_index);
				if((saved_un.equals(un))&&(saved_dm.equals(dm)))
				{
					Toast.makeText(instance, "Account already exists.", Toast.LENGTH_LONG).show();
				}else
				{


					
					if ((un != null) && (pw != null) && (dm != null)) {
						// delete existing account if any
						if (prefs.getAccountCount() > 0) {
							prefs.deleteAccount(index);
						}
						
						// if this is the only account, it will be set as default
						checkAccount(un, pw, dm);
						
						int din = prefs.getDefaultAccountIndex();
						int ndin = prefs.getNonDefaultAccountIndex();
						
						db.setLinphoneAccountId(0, din);
						db.setLinphoneAccountId(1, ndin);
						
						if(accountCreated) {
							db.markAccountAsSet(settingsId);
							success();
						} else {
							Toast.makeText(instance, "Error in creating account.", Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(instance, "Incomplete settings.", Toast.LENGTH_LONG).show();
					}
				
				}
			}
		});
		
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	private void logIn(String username, String password, String domain, boolean sendEcCalibrationResult) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null && getCurrentFocus() != null) {
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		}

        saveCreatedAccount(username, password, domain);
	}
	
	private LinphoneOnRegistrationStateChangedListener registrationListener = new LinphoneOnRegistrationStateChangedListener() {
		public void onRegistrationStateChanged(RegistrationState state) {
			if (state == RegistrationState.RegistrationOk) {
				LinphoneManager.removeListener(registrationListener);
			} else if (state == RegistrationState.RegistrationFailed) {
				LinphoneManager.removeListener(registrationListener);
				mHandler.post(new Runnable () {
					public void run() {
						Toast.makeText(RegistrationActivity.this, getString(R.string.first_launch_bad_login_password), Toast.LENGTH_LONG).show();
					}
				});
			}
		}
	};
	
	public void checkAccount(String username, String password, String domain) {
		LinphoneManager.removeListener(registrationListener);
		LinphoneManager.addListener(registrationListener);
		
		saveCreatedAccount(username, password, domain);
	}

	public void linphoneLogIn(String username, String password, boolean validate) {
		if (validate) {
			checkAccount(username, password, getString(R.string.default_domain));
		} else {
			logIn(username, password, getString(R.string.default_domain), true);
		}
	}
	
	public void genericLogIn(String username, String password, String domain) {
		logIn(username, password, domain, false);
	}
	
	public void isAccountVerified() {
		Toast.makeText(this, getString(R.string.setup_account_validated), Toast.LENGTH_LONG).show();
	}

	public void isEchoCalibrationFinished() {
		success();
	}
	
	public void success() {
		prefs.firstLaunchSuccessful();
		setResult(Activity.RESULT_OK);
		finish();
	}
	
	public void saveCreatedAccount(String username, String password, String domain) {
		if (accountCreated)
			return;
		
		boolean isMainAccountLinphoneDotOrg = domain.equals(getString(R.string.default_domain));
		boolean useLinphoneDotOrgCustomPorts = getResources().getBoolean(R.bool.use_linphone_server_ports);
		prefs.setNewAccountUsername(username);
		prefs.setNewAccountDomain(domain);
		prefs.setNewAccountPassword(password);
		
		if (isMainAccountLinphoneDotOrg && useLinphoneDotOrgCustomPorts) {
			if (getResources().getBoolean(R.bool.disable_all_security_features_for_markets)) {
				prefs.setNewAccountProxy(domain + ":5228");
				prefs.setTransport(getString(R.string.pref_transport_tcp_key));
			}
			else {
				prefs.setNewAccountProxy(domain + ":5223");
				prefs.setTransport(getString(R.string.pref_transport_tls_key));
			}
			
			prefs.setNewAccountExpires("604800");
			prefs.setNewAccountOutboundProxyEnabled(true);
			prefs.setStunServer(getString(R.string.default_stun));
			prefs.setIceEnabled(true);
			prefs.setPushNotificationEnabled(true);
		} else {
			String forcedProxy = getResources().getString(R.string.setup_forced_proxy);
			if (!TextUtils.isEmpty(forcedProxy)) {
				prefs.setNewAccountProxy(forcedProxy);
				prefs.setNewAccountOutboundProxyEnabled(true);
			}
		}
		
		if (getResources().getBoolean(R.bool.enable_push_id)) {
			String regId = prefs.getPushNotificationRegistrationID();
			String appId = getString(R.string.push_sender_id);
			if (regId != null && prefs.isPushNotificationEnabled()) {
				String contactInfos = "app-id=" + appId + ";pn-type=google;pn-tok=" + regId;
				prefs.setNewAccountContactParameters(contactInfos);
			}
		}
		
		try {
			prefs.saveNewAccount();
			accountCreated = true;
		} catch (LinphoneCoreException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is
		// present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}
}
