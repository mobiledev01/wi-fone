package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class contactsListAdapter extends ArrayAdapter<String []>
{
	private ArrayList<String []> objects;
	public contactsListAdapter(Context context,
			int textViewResourceId, ArrayList<String []> objects)
	{
		super(context, textViewResourceId, objects);
		this.objects = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if(v==null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.contacts_list_adapter, null);
		}
		
		String[] contact = objects.get(position);
		if(contact!=null)
		{
			TextView contactNumber = (TextView) v.findViewById(R.id.ContactNumber);
			TextView contactName = (TextView) v.findViewById(R.id.ContactName);
			TextView contactId = (TextView) v.findViewById(R.id.ContactId);
			//Log.i("PTCLSIP:", contact[0]+" : "+contact[1]+" : "+contact[2]);
			contactId.setText(contact[0]);
			contactName.setText(contact[1]);
			contactNumber.setText(contact[2]);
		}
		return v;
	}
}

