package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper{
	private static DatabaseHandler instance;
	
	private final static String DATABASE_NAME = "PTCL_SIP_DATABASE.db";
	private final static String CALL_LOG = "CALL_LOG";
	private final static String WF_CONTACTS = "WF_CONTACTS";
	private final static String CONVERSATIONS = "CONVERSATIONS";
	private final static String MESSAGES = "MESSAGES";
	private final static String LP_ACCOUNTS = "LP_ACCOUNTS";
	//private final static String SIP_SETTINGS = "SIP_SETTINGS";
	
	private DatabaseHandler(Context context){
		super(context, DATABASE_NAME, null, 10);
	}
	
	public static DatabaseHandler getInstance(Context context) {
		if (instance == null) {
			instance = new DatabaseHandler(context);
		}
		
		return instance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LP_ACCOUNTS = "CREATE TABLE " + LP_ACCOUNTS + " (acc_id INTEGER PRIMARY KEY NOT NULL, lp_id INTEGER, is_set INTEGER)";
		String CREATE_CALL_LOG = "CREATE TABLE " + CALL_LOG + "(log_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, number TEXT, start_time INTEGER, end_time INTEGER, type INTEGER, status TEXT, config_id INTEGER)";
		String CREATE_WF_CONTACTS = "CREATE TABLE " + WF_CONTACTS + " (wfc_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, first_name TEXT, last_name TEXT, username TEXT, domain TEXT)";
		String CREATE_CONVERSATIONS = "CREATE TABLE " + CONVERSATIONS + " (c_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username TEXT, last_activity_time INTEGER, is_read INTEGER)";
		String CREATE_MESSAGES = "CREATE TABLE " + MESSAGES + " (msg_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, c_id INTEGER, direction INTEGER, message TEXT, timestamp INTEGER, is_read INTEGER)";
		
		db.execSQL(CREATE_LP_ACCOUNTS);
		db.execSQL(CREATE_CALL_LOG);
		db.execSQL(CREATE_WF_CONTACTS);
		db.execSQL(CREATE_CONVERSATIONS);
		db.execSQL(CREATE_MESSAGES);
		
		db.execSQL("INSERT INTO LP_ACCOUNTS (acc_id, lp_id, is_set) VALUES (0, 0, 0)");
		db.execSQL("INSERT INTO LP_ACCOUNTS (acc_id, lp_id, is_set) VALUES (1, 1, 0)");
		
		//String CREATE_SIP_SETTINGS = "CREATE TABLE " + SIP_SETTINGS + "(settings_id INTEGER PRIMARY KEY NOT NULL, server_ip TEXT, server_proxy TEXT, username TEXT, password TEXT, type INTEGER)";
		//db.execSQL(CREATE_SIP_SETTINGS);
		//db.execSQL("INSERT INTO SIP_SETTINGS (settings_id, server_ip, username, password) VALUES (0, '192.168.1.1', 'admin', 'admin')");
		//db.execSQL("INSERT INTO SIP_SETTINGS (settings_id, server_ip, username, password) VALUES (1, '192.168.1.2', 'admin', 'admin')");
//		db.execSQL("INSERT INTO CALL_LOG (number, start_time, end_time, type, status, config_id) VALUES ('+923125052717', 1386698650, 1386698700, 1, 1, 0)");
//		db.execSQL("INSERT INTO CALL_LOG (number, start_time, end_time, type, status, config_id) VALUES ('+923122342424', 1386698550, 1386698700, 1, 1, 0)");
//		db.execSQL("INSERT INTO CALL_LOG (number, start_time, end_time, type, status, config_id) VALUES ('+923122426657', 1386697650, 1386698700, 1, 1, 0)");
//		db.execSQL("INSERT INTO CALL_LOG (number, start_time, end_time, type, status, config_id) VALUES ('+923125675675', 1386696650, 1386698700, 1, 1, 0)");
//		db.execSQL("INSERT INTO CALL_LOG (number, start_time, end_time, type, status, config_id) VALUES ('+923125679867', 1386699650, 1386698700, 1, 1, 0)");
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + LP_ACCOUNTS);
		db.execSQL("DROP TABLE IF EXISTS " + CALL_LOG);
		db.execSQL("DROP TABLE IF EXISTS " + WF_CONTACTS);
		db.execSQL("DROP TABLE IF EXISTS " + CONVERSATIONS);
		db.execSQL("DROP TABLE IF EXISTS " + MESSAGES);
        onCreate(db);
	}
	
	public int getLinphoneAccountId(int accountId) {
		String selectQuery = "SELECT * FROM " + LP_ACCOUNTS + " WHERE acc_id = '" + accountId + "' LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		int accId = 0;
		
		if (cursor.moveToFirst()) {
			do {
				accId = cursor.getInt(cursor.getColumnIndex("lp_id"));
//				if(accId==-1)
//				{
//					accId=0;
//				}
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return accId;
	}
	
	public int setLinphoneAccountId(int accountId, int LpAccountId) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("lp_id", LpAccountId);
		int result = db.update(LP_ACCOUNTS, values, "acc_id = " + accountId, null);
		Log.i("DB: WF_CONTACTS;","Updated " + Integer.toBinaryString(result) + " rows");
		
		db.close();
		return result;
	}
	
	public int isAccountSet(int accountId) {
		String selectQuery = "SELECT * FROM " + LP_ACCOUNTS + " WHERE acc_id = '" + accountId + "' LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		int isSet = 0;
		
		if (cursor.moveToFirst()) {
			do {
				isSet = cursor.getInt(cursor.getColumnIndex("is_set"));
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return isSet;
	}
	
	public int markAccountAsSet(int accountId) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("is_set", 1);
		int result = db.update(LP_ACCOUNTS, values, "acc_id = " + accountId, null);
		Log.i("DB: WF_CONTACTS;","Updated " + Integer.toBinaryString(result) + " rows");
		
		db.close();
		return result;
	}
	
	public int createNewWiFoneContact(String firstName, String lastName, String username, String domain) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("first_name", firstName);
		values.put("last_name", lastName);
		values.put("username", username);
		values.put("domain", domain);
		int id = (int) db.insert(WF_CONTACTS, null, values);
		
		db.close();
		return id;
	}
	
	public int updateWiFoneContact(int contactId, String firstName, String lastName, String username, String domain) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("first_name", firstName);
		values.put("last_name", lastName);
		values.put("username", username);
		values.put("domain", domain);
		int result = db.update(WF_CONTACTS, values, "wfc_id = " + contactId, null);
		Log.i("DB: WF_CONTACTS;","Updated " + Integer.toBinaryString(result) + " rows");
		
		db.close();
		return result;
	}
	
	public int deleteWiFoneContact(int contactId) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete(WF_CONTACTS, "wfc_id = " + contactId, null);
		
		db.close();
		return result;
	}
	
	public WiFoneContact getWiFoneContact(int contactId) {
		String selectQuery = "SELECT * FROM " + WF_CONTACTS + " WHERE wfc_id = '" + contactId + "' LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		WiFoneContact contact = null;
		
		if (cursor.moveToFirst()) {
			do {
	        	contact = new WiFoneContact();
	        	contact.wiFoneContactId = cursor.getInt(cursor.getColumnIndex("wfc_id"));
	        	contact.firstName = cursor.getString(cursor.getColumnIndex("first_name"));
	        	contact.lastName = cursor.getString(cursor.getColumnIndex("last_name"));
	        	contact.username = cursor.getString(cursor.getColumnIndex("username"));
	        	contact.domain = cursor.getString(cursor.getColumnIndex("domain"));
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return contact;
	}
	
	public WiFoneContact getWiFoneContact(String username) {
		String selectQuery = "SELECT * FROM " + WF_CONTACTS + " WHERE username = '" + username + "' LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		WiFoneContact contact = null;
		
		if (cursor.moveToFirst()) {
			do {
	        	contact = new WiFoneContact();
	        	contact.wiFoneContactId = cursor.getInt(cursor.getColumnIndex("wfc_id"));
	        	contact.firstName = cursor.getString(cursor.getColumnIndex("first_name"));
	        	contact.lastName = cursor.getString(cursor.getColumnIndex("last_name"));
	        	contact.username = cursor.getString(cursor.getColumnIndex("username"));
	        	contact.domain = cursor.getString(cursor.getColumnIndex("domain"));
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return contact;
	}
	
	public ArrayList<WiFoneContact> getAllWiFoneContacts() {
		ArrayList<WiFoneContact> tempArray = new ArrayList<WiFoneContact>();
		String selectQuery = "SELECT  * FROM " + WF_CONTACTS + " ORDER BY username";
		SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	    Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
	    if (cursor.moveToFirst()) {
	        do {
	        	WiFoneContact contact = new WiFoneContact();
	        	contact.wiFoneContactId = cursor.getInt(cursor.getColumnIndex("wfc_id"));
	        	contact.username = cursor.getString(cursor.getColumnIndex("username"));
	        	contact.firstName = cursor.getString(cursor.getColumnIndex("first_name"));
	        	contact.lastName = cursor.getString(cursor.getColumnIndex("last_name"));
	        	contact.domain = cursor.getString(cursor.getColumnIndex("domain"));
	        	tempArray.add(contact);
	        } while (cursor.moveToNext());
	    }
	    
	    db.close();
		return tempArray;
	}

	public ArrayList<Conversation> getAllConversations() {
		ArrayList<Conversation> tempArray = new ArrayList<Conversation>();
		String selectQuery = "SELECT  * FROM " + CONVERSATIONS + " ORDER BY last_activity_time DESC";
		SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	    Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
	    if (cursor.moveToFirst()) {
	        do {
	        	Conversation conversation = new Conversation();
	        	conversation.conversationId = cursor.getInt(cursor.getColumnIndex("c_id"));
	        	conversation.username = cursor.getString(cursor.getColumnIndex("username"));
	        	conversation.isRead = cursor.getInt(cursor.getColumnIndex("is_read"));
	        	conversation.lastActivityTime = cursor.getInt(cursor.getColumnIndex("last_activity_time"));
	        	
	        	tempArray.add(conversation);
	        } while (cursor.moveToNext());
	    }
	    
	    db.close();
		return tempArray;
	}
	
	public Conversation findExistingConversation(String username) {
		String selectQuery = "SELECT * FROM " + CONVERSATIONS + " WHERE username = '" + username + "' LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		Conversation conversation = null;
		
		if (cursor.moveToFirst()) {
			do {
	        	conversation = new Conversation();
	        	conversation.conversationId = cursor.getInt(cursor.getColumnIndex("c_id"));
	        	conversation.username = cursor.getString(cursor.getColumnIndex("username"));
	        	conversation.isRead = cursor.getInt(cursor.getColumnIndex("is_read"));
	        	conversation.lastActivityTime = cursor.getInt(cursor.getColumnIndex("last_activity_time"));
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return conversation;
	}
	
	public int createNewConversation(String username) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("username", username);
		values.put("last_activity_time", (int) (System.currentTimeMillis()/1000));
		values.put("is_read", 0);
		int id = (int) db.insert(CONVERSATIONS, null, values);
		
		db.close();
		return id;
	}
	
	public int deleteConversation(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete(CONVERSATIONS, "c_id = " + id, null);
		
		db.close();
		return result;
	}
	
	public int deleteConversation(String username) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete(CONVERSATIONS, "username = " + username, null);
		
		db.close();
		return result;
	}
	
	public int updateConversationAsRead(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("is_read", 1);
		int result = db.update(CONVERSATIONS, values, "c_id = " + id, null);
		Log.i("DB: CONVERSATIONS;","Updated " + Integer.toBinaryString(result) + " rows");
		
		db.close();
		return result;
	}
	
	public int updateConversationLastActivityTime(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("last_activity_time", (int)(System.currentTimeMillis()/1000));
		int result = db.update(CONVERSATIONS, values, "c_id = " + id, null);
		Log.i("DB: CONVERSATIONS;","Updated " + Integer.toBinaryString(result) + " rows");
		
		db.close();
		return result;
	}
	
	public int createNewMessage(int conversationId, int direction, String message, int timestamp, int isRead) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("c_id", conversationId);
		values.put("direction", direction);
		values.put("message", message);
		values.put("timestamp", timestamp);
		values.put("is_read", isRead);
		int id = (int) db.insert(MESSAGES, null, values);
		
		db.close();
		return id;
	}
	
	public Message getMessage(int msgId) {
		String selectQuery = "SELECT * FROM " + MESSAGES + " WHERE msg_id = '" + msgId + "' LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		Message msg = null;
		
		if (cursor.moveToFirst()) {
			do {
	        	msg = new Message();
	        	msg.messageId = cursor.getInt(cursor.getColumnIndex("msg_id"));
	        	msg.conversationId = cursor.getInt(cursor.getColumnIndex("c_id"));
	        	msg.direction = cursor.getInt(cursor.getColumnIndex("direction"));
	        	msg.message = cursor.getString(cursor.getColumnIndex("message"));
	        	msg.timestamp = cursor.getInt(cursor.getColumnIndex("timestamp"));
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return msg;
	}
	
	public Message getLatestMessage(int conversationId) {
		String selectQuery = "SELECT * FROM " + MESSAGES + " WHERE c_id = '" + conversationId + "' ORDER BY timestamp DESC LIMIT 1";
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
		
		Message msg = null;
		
		if (cursor.moveToFirst()) {
			do {
	        	msg = new Message();
	        	msg.messageId = cursor.getInt(cursor.getColumnIndex("msg_id"));
	        	msg.conversationId = cursor.getInt(cursor.getColumnIndex("c_id"));
	        	msg.direction = cursor.getInt(cursor.getColumnIndex("direction"));
	        	msg.message = cursor.getString(cursor.getColumnIndex("message"));
	        	msg.timestamp = cursor.getInt(cursor.getColumnIndex("timestamp"));
	        } while (cursor.moveToNext());
		}
		
		db.close();
		return msg;
	}
	
	public int deleteMessage(int messageId) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete(MESSAGES, "msg_id = " + messageId, null);
		
		db.close();
		return result;
	}
	
	public int deleteConversationMessages(int conversationId) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete(MESSAGES, "c_id = " + conversationId, null);
		
		db.close();
		return result;
	}
	
	public ArrayList<Message> getConversationMessages(int conversationId) {
		ArrayList<Message> tempArray = new ArrayList<Message>();
		String selectQuery = "SELECT  * FROM " + MESSAGES + " WHERE c_id = " + conversationId + " ORDER BY timestamp";
		SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	    Log.i("DB: Rows; ", Integer.toString(cursor.getCount()));
	    if (cursor.moveToFirst()) {
	        do {
	        	Message m = new Message();
	        	m.messageId = cursor.getInt(cursor.getColumnIndex("msg_id"));
	        	m.conversationId = cursor.getInt(cursor.getColumnIndex("c_id"));
	        	m.direction = cursor.getInt(cursor.getColumnIndex("direction"));
	        	m.message = cursor.getString(cursor.getColumnIndex("message"));
	        	m.timestamp = cursor.getInt(cursor.getColumnIndex("timestamp"));
	        	tempArray.add(m);
	        } while (cursor.moveToNext());
	    }
	    
	    db.close();
		return tempArray;
	}
	
	public int getUnreadMessageCount(int conversationId) {
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT COUNT (*) FROM " + MESSAGES + " WHERE is_read = 0 AND c_id = " + conversationId;
		Cursor cursor= db.rawQuery(selectQuery, null);
		int count = 0;
		if(null != cursor) {
			if(cursor.getCount() > 0){
				cursor.moveToFirst();    
				count = cursor.getInt(0);
		    }
		    cursor.close();
		}
		
		db.close();
		return count;
	}
	
	public int markMessagesAsRead(int conversationId) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("is_read", 1);
		int result = db.update(MESSAGES, values, "c_id = " + conversationId, null);
		Log.i("DB: MESSAGES;","Updated " + Integer.toBinaryString(result) + " rows");
		
		db.close();
		return result;
	}
	
	public int enterLog(String number, int start_time, int end_time, int type, int status, int config_id){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("number", number);
		values.put("start_time", start_time);
		values.put("end_time", end_time);
		values.put("type", type);
		values.put("status", status);
		values.put("config_id", config_id);
		int id = (int) db.insert(CALL_LOG, null, values);
		
		db.close();
		return id;
	}
	
	public ArrayList<CallLogItem> getCallLog(){
		try{
		
		Log.i("ICE TEST","TESTING");
		ArrayList<CallLogItem> tempArray = new ArrayList<CallLogItem>();
		String selectQuery = "SELECT  * FROM " + CALL_LOG + " ORDER BY log_id DESC LIMIT 15";
		SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	    
	    Log.i("DB: Rows Test ICE; ", Integer.toString(cursor.getCount()));
	    if (cursor.moveToFirst()) {
	        do {
	        	CallLogItem item = new CallLogItem();
	        	item.logId = 0;
	        	item.number = cursor.getString(cursor.getColumnIndex("number"));
	        	item.startTime = cursor.getInt(cursor.getColumnIndex("start_time"));
	        	item.endTime = cursor.getInt(cursor.getColumnIndex("end_time"));
	        	item.type = cursor.getInt(cursor.getColumnIndex("type"));
	        	item.status = cursor.getString(cursor.getColumnIndex("status"));
	        	item.configId = cursor.getInt(cursor.getColumnIndex("config_id"));
	        	tempArray.add(item);
	        } while (cursor.moveToNext());
	    }
	    
	    Log.d("ICE DEBUG",tempArray.get(0).number);
	    db.close();
		return tempArray;
		}
		catch(Exception e)
		{
			Log.e("error", e.toString());
			return null;
		}
	}
	
	public int updateCallEndTime(int callId, int time){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("end_time", time);
		int ret = db.update(CALL_LOG, values, "log_id = " + callId, null);
		Log.i("DB: CALL_LOG;","Updated " + Integer.toBinaryString(ret) + " rows");
		
		db.close();
		return ret;
	}
	public int updateCallStatus(int callId, int status){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("status", status);
		int ret = db.update(CALL_LOG, values, "log_id = " + callId, null);
		Log.i("DB: CALL_LOG;","Updated " + Integer.toBinaryString(ret) + " rows");
		
		db.close();
		return ret;
	}
	public int updateCallStartTime(int callId, int time){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("start_time", time);
		int ret = db.update(CALL_LOG, values, "log_id = " + callId, null);
		Log.i("DB: CALL_LOG;","Updated " + Integer.toBinaryString(ret) + " rows");
		
		db.close();
		return ret;
	}
	
	public class CallLogItem {
		int logId;
		String number;
		int startTime;
		int endTime;
		int type;			// 0 for incoming, 1 for outgoing
		String status;		//0 for initiated, 1 for connected,2 for end,3 for call error,4 for idle
		int configId;
	}
	
	public class WiFoneContact {
		public int wiFoneContactId;
		public String firstName;
		public String lastName;
		public String username;
		public String domain;
	}
	
	public class Conversation {
		public int conversationId;
		public String username;
		public int isRead;
		public int lastActivityTime;
	}
	
	public class Message {
		public int messageId;
		public int conversationId;
		public String message;
		public int direction;
		public int timestamp;
	}
}
