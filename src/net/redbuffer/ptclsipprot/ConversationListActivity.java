package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.mediastream.Log;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ConversationListActivity extends Activity {
	public static String conversationId;
	public static String username;
	private static ConversationListActivity instance;
	private DatabaseHandler db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conversation_list);
		try
		{
			instance = this;
			db = DatabaseHandler.getInstance(getBaseContext());
			final ArrayList<DatabaseHandler.Conversation> conversationsList = db.getAllConversations();
			final ListView conversationList = (ListView) findViewById(R.id.conversation_list_view);
			//ConversationListViewAdapter arrayAdapter = new ConversationListViewAdapter(this, R.layout.conversation_listview_adapter, conversationsList);
			ConversationListViewAdapter arrayAdapter = new ConversationListViewAdapter(this, android.R.layout.simple_list_item_1, conversationsList);
			conversationList.setAdapter(arrayAdapter);
			conversationList.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					DatabaseHandler.Conversation conversation = conversationsList.get(position);
					username = conversation.username;
					
					Intent intent = new Intent(getBaseContext(), ConversationActivity.class);
					intent.putExtra("ConversationActivity.conversationId", conversation.conversationId);
					intent.putExtra(ConversationActivity.username, username);
					startActivity(intent);
				}
			});
		}catch(Exception e)
		{
			Log.d("error in calling activity",e.toString());
			exceptionHandler(e.toString());
		}
	}
	
	public static boolean isInstantiated() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public static ConversationListActivity getInstance() {
		return instance;
	}
	
	@Override
	protected void onDestroy() {
		instance = null;
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conversation_list, menu);
		return true;
	}
	
	public void onMessageReceived(final int id, LinphoneAddress from, final LinphoneChatMessage message) {
		// TODO: act as a new message arrives
	}
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(this, error, Toast.LENGTH_LONG);
		toast.show();
	}
}
