package net.redbuffer.ptclsipprot;

import org.linphone.CallManager;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.LinphoneSimpleListener.LinphoneOnCallStateChangedListener;
import org.linphone.StatusFragment;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallParams;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCoreException;
import org.linphone.mediastream.Log;
import org.linphone.mediastream.video.AndroidVideoWindowImpl;
import org.linphone.mediastream.video.capture.hwconf.AndroidCameraConfiguration;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CallingActivity extends Activity implements LinphoneOnCallStateChangedListener {
    
	private static CallingActivity instance;
	public static int configId = 0;
	public ToneGenerator toneGenerator;
	public ToneGenerator ringtoneGenerator;
	Button btnCancelCall, btnToggleSpeaker, btnIncomingCallToggle, btnEnableVideo;
	DatabaseHandler db = null;
	LinphoneCall mCall;
	TextView phoneNumber;
	int callId = 0;
	boolean is_video_call=false;
	boolean isIncomingCallPicked = false;
	boolean videoRequested = false;
	
    public String sipAddress = "";
    
    private SurfaceView mVideoView;
	private SurfaceView mCaptureView;
	private AndroidVideoWindowImpl androidVideoWindowImpl;
    
	public static CallingActivity getInstance() {
		return instance;
	}
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calling);
		
		db = DatabaseHandler.getInstance(this);
		
		Intent intent = getIntent();
		sipAddress = intent.getStringExtra(dialerFragment.sipAddress);
		configId = intent.getIntExtra("CallingActivity.configId", 0);
		is_video_call = intent.getBooleanExtra("is_video_call", false);		
		
		instance = this;
		toneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 40);
		ringtoneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 100);
		
		btnCancelCall = (Button) findViewById(R.id.btnCloseCall);
		btnIncomingCallToggle = (Button) findViewById(R.id.btnIncomingCallToggle);
		btnToggleSpeaker = (Button) findViewById(R.id.btnToggleSpeaker);
		btnEnableVideo = (Button) findViewById(R.id.btnEnableVideo);
		
		try
		{
			LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(configId));
			phoneNumber = (TextView) findViewById(R.id.phoneNumber);
			phoneNumber.setVisibility(View.VISIBLE);
			if (sipAddress == null || sipAddress == "") {
				// this must be an incoming call
				btnCancelCall.setVisibility(Button.VISIBLE);
				btnIncomingCallToggle.setVisibility(Button.VISIBLE);
			} else {
				Log.d("audio call","call");
				btnCancelCall.setVisibility(Button.VISIBLE);
				btnIncomingCallToggle.setVisibility(Button.GONE);
				phoneNumber.setText(sipAddress);
				initiateCall(sipAddress);
			}
		} catch(Exception e) {
			Log.d("error in calling activity",e.toString());
			exceptionHandler(e.toString());
		}
	}
	
	private void initiateCall(String callee) {
		LinphoneAddress addr;
		CallManager cm = CallManager.getInstance();
		
		try {
			addr = LinphoneManager.getLc().interpretUrl(sipAddress);
			cm.inviteAddress(addr, is_video_call, false);
		} catch (LinphoneCoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		LinphoneManager.getInstance().newOutgoingCall(callee, callee);
		Log.i("IntiateCall: " + sipAddress);
		int time = (int) (System.currentTimeMillis()/1000);
		db.enterLog(sipAddress, time, 0, 1, 0, configId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.calling, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		instance = null;
		LinphoneManager.getInstance().terminateCall();
		Log.d("onBackPressed: ", "terminate call invoked.");
		super.onBackPressed();
	}
	
	public void closeCall(View view) {
		toneGenerator.stopTone();
		ringtoneGenerator.stopTone();
		LinphoneManager.getInstance().terminateCall();
		Log.d("closeCall: "+ sipAddress);
	}
	
	public void toggleIncomingCall(View view) {
		if (isIncomingCallPicked) {
			LinphoneManager.getInstance().terminateCall();
			btnIncomingCallToggle.setText("OK");
			isIncomingCallPicked = false;
		} else {
			LinphoneManager.getInstance().acceptCall(mCall);
			btnIncomingCallToggle.setText("End");
			btnCancelCall.setVisibility(Button.GONE);
			isIncomingCallPicked = true;
		}
	}
	
	public void updateCallStatus(final String status) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				TextView statusView = (TextView) findViewById(R.id.callStatus);
				statusView.setText(status);
			}
        });
	}
	
	public void toggleSpeakerDuringCall(View view) {
		Button toggleSpeaker = (Button) view;
		LinphoneCore lc = LinphoneManager.getLc();
		if ((lc != null) && (lc.isIncall())) {
			if (lc.isSpeakerEnabled()) {
				lc.enableSpeaker(false);
				toggleSpeaker.setText("Turn Speaker ON");
			} else {
				lc.enableSpeaker(true);
				toggleSpeaker.setText("Turn Speaker OFF");
			}
		}
	}
	
	@Override
	public void onCallStateChanged(LinphoneCall call, State state, String message) {
		Log.i("dispatched state [" + state + "]");
		
		try
		{
			if(state == State.IncomingReceived) {
				LinphoneCallParams p = call.getRemoteParams();
				//p.setVideoEnabled(true);
				//ringtoneGenerator.startTone(ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK);
				//ringtoneGenerator.startTone(ToneGenerator.TONE_CDMA_CALL_SIGNAL_ISDN_NORMAL);
				//ringtoneGenerator.startTone(ToneGenerator.TONE_CDMA_CALL_SIGNAL_ISDN_PING_RING);
				ringtoneGenerator.startTone(ToneGenerator.TONE_CDMA_HIGH_PBX_S_X4);
				
				if (p.getVideoEnabled()) {
					updateCallStatus("Incoming video call...");
				} else {
					updateCallStatus("Incoming voice call...");
				}
				
				LinphoneAddress callingAddress = call.getRemoteAddress();
				if (callingAddress.getDisplayName() != null) {
					sipAddress = callingAddress.getDisplayName();
				} else if (callingAddress.getUserName() != null) {
					sipAddress = callingAddress.getUserName();
				} else {
					sipAddress = "Unknown";
				}
				
				phoneNumber.setText(sipAddress);
				this.mCall = call;
				
				// TODO: get info about the server from database and set configId accordingly
				
				int time = (int) (System.currentTimeMillis()/1000);
				callId = db.enterLog(sipAddress, time, time, 0, 0, configId);
			} else if (state == State.OutgoingInit) {
				updateCallStatus("Calling...");
			} else if (state == State.OutgoingRinging) {
				updateCallStatus("Ringing...");
				toneGenerator.startTone(ToneGenerator.TONE_CDMA_NETWORK_USA_RINGBACK);
			} else if (state == State.Connected) {
				LinphoneCallParams p = call.getCurrentParamsCopy();
				//p.setVideoEnabled(true);
				
				if (p.getVideoEnabled()) {
					videoSetup();
				}
				
				ringtoneGenerator.stopTone();
				toneGenerator.stopTone();
				btnCancelCall.setText("End");
				updateCallStatus("Call connected.");
				int time = (int) (System.currentTimeMillis()/1000);
				db.updateCallStartTime(callId, time);
				db.updateCallStatus(callId, 1);
				if(is_video_call)
			    {
					LinphoneManager.reinviteWithVideo();
			    } else {
			    	btnEnableVideo.setVisibility(View.VISIBLE);
			    }
			} else if (state == State.CallUpdatedByRemote) {
				LinphoneCallParams params = call.getRemoteParams();
				if (params.getVideoEnabled()) {
					call.enableCamera(true);
					LinphoneManager.getLc().acceptCallUpdate(call, params);
					btnEnableVideo.setVisibility(View.GONE);
					videoSetup();
					Log.d("RemoteVideo: ", "enabled");
				} else {
					Log.d("RemoteVideo: ", "not enabled");
				}
			} else if (state == State.StreamsRunning) {
				if (videoRequested) {
					btnEnableVideo.setVisibility(View.GONE);
					videoSetup();
				}
				
				if (is_video_call) {
					videoRequested = true;
				}
			} else if (state == State.OutgoingProgress) {
				updateCallStatus("Call in progress.");
			} else if (state == State.Paused) {
				updateCallStatus("Call on hold.");
			} else if (state == State.PausedByRemote) {
				updateCallStatus("Call on hold.");
			} else if (state == State.CallEnd) {
				updateCallStatus("Call ended.");
				LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(0));
				ringtoneGenerator.stopTone();
				toneGenerator.stopTone();
				int time = (int) (System.currentTimeMillis()/1000);
				db.updateCallEndTime(callId, time);
				db.updateCallStatus(callId, 2);
				db.close();
				instance = null;
				finish();
			} else if (state == State.Error) {
				updateCallStatus("Error in call.");
				LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(0));
				ringtoneGenerator.stopTone();
				toneGenerator.stopTone();
				db.updateCallStatus(callId, 3);
				updateCallStatus("Error in call.");
			} else if (state == State.CallReleased) {
				updateCallStatus("Call released.");
				LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(0));
				ringtoneGenerator.stopTone();
				toneGenerator.stopTone();
			} else if (state == State.Idle) {
				updateCallStatus("Idle.");
				LinphonePreferences.instance().setDefaultAccount(db.getLinphoneAccountId(0));
				ringtoneGenerator.stopTone();
				toneGenerator.stopTone();
				db.updateCallStatus(callId, 4);
			}
		} catch(Exception e) {
			Log.d("error in calling activity",e.toString());
			exceptionHandler(e.toString());
		}
	}

	public void updateStatusFragment(StatusFragment statusFragment) {
		updateCallStatus(statusFragment.getStatusText());
	}
	
	public void videoSetup()
	{
		Log.d("Video Support: ", LinphoneManager.getLc().isVideoSupported());
		Log.d("Total Cameras: ", AndroidCameraConfiguration.retrieveCameras().length);
		Log.d("Active Camera: ", LinphoneManager.getLc().getVideoDevice());
		
		RelativeLayout videoCallView = (RelativeLayout) findViewById(R.id.video_layout);
		videoCallView.setVisibility(View.VISIBLE);
		mVideoView = (SurfaceView) findViewById(R.id.videoSurface);
		mCaptureView = (SurfaceView) findViewById(R.id.videoCaptureSurface);
		
		mVideoView.setVisibility(View.VISIBLE);
		mCaptureView.setVisibility(View.VISIBLE);
		
		mCaptureView.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); // Warning useless because value is ignored and automatically set by new APIs.
		
		fixZOrder(mVideoView, mCaptureView);
		
		androidVideoWindowImpl = new AndroidVideoWindowImpl(mVideoView, mCaptureView);
		androidVideoWindowImpl.setListener(new AndroidVideoWindowImpl.VideoWindowListener() {
			public void onVideoRenderingSurfaceReady(AndroidVideoWindowImpl vw, SurfaceView surface) {
				LinphoneManager.getLc().setVideoWindow(vw);
				mVideoView = surface;
			}
			
			public void onVideoRenderingSurfaceDestroyed(AndroidVideoWindowImpl vw) {
				LinphoneCore lc = LinphoneManager.getLc(); 
				if (lc != null) {
					lc.setVideoWindow(null);
				}
			}
			
			public void onVideoPreviewSurfaceReady(AndroidVideoWindowImpl vw, SurfaceView surface) {
				mCaptureView = surface;
				LinphoneManager.getLc().setPreviewWindow(mCaptureView);
			}
			
			public void onVideoPreviewSurfaceDestroyed(AndroidVideoWindowImpl vw) {
				// Remove references kept in jni code and restart camera
				LinphoneManager.getLc().setPreviewWindow(null);
			}
		});
		
		androidVideoWindowImpl.init();	
	}
	
	private void fixZOrder(SurfaceView video, SurfaceView preview) {
		video.setZOrderOnTop(false);
		preview.setZOrderOnTop(true);
		preview.setZOrderMediaOverlay(true); // Needed to be able to display control layout over
	}
	
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(this, error, Toast.LENGTH_LONG);
		toast.show();
	}
	
	public void enableVideoDuringCall(View view) {
		//LinphoneCallParams params = this.call.getCurrentParamsCopy();
		//LinphoneCallParams remoteParams = this.call.getRemoteParams();
		
		//LinphoneManager.getLc().updateCall(this.call, params);
		LinphoneManager.reinviteWithVideo();
		videoRequested = true;
	}
}
