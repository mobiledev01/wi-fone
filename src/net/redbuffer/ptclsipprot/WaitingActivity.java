package net.redbuffer.ptclsipprot;

import static android.content.Intent.ACTION_MAIN;

import org.linphone.LinphoneService;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

public class WaitingActivity extends Activity {

	private Handler handler;
	private ServiceWaitThread thread;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_waiting);
		
		handler = new Handler();
		
		if (LinphoneService.isReady()) {
			onServiceReady();
		} else {
			startService(new Intent(ACTION_MAIN).setClass(this, LinphoneService.class));
			thread = new ServiceWaitThread();
			thread.start();
		}
	}
	
	private void onServiceReady() {
		final Class<? extends Activity> classToStart;
		
		if (getResources().getBoolean(R.bool.show_tutorials_instead_of_app)) {
			classToStart = RegistrationActivity.class;
		} else {
			classToStart = MainActivity.class;
		}
		
		LinphoneService.instance().setActivityToLaunchOnIncomingReceived(classToStart);
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				startActivity(new Intent().setClass(WaitingActivity.this, classToStart).setData(getIntent().getData()));
				finish();
			}
		}, 1000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.waiting, menu);
		return true;
	}
	
	private class ServiceWaitThread extends Thread {
		public void run() {
			while (!LinphoneService.isReady()) {
				try {
					sleep(30);
				} catch (InterruptedException e) {
					throw new RuntimeException("waiting thread sleep() has been interrupted");
				}
			}

			handler.post(new Runnable() {
				@Override
				public void run() {
					onServiceReady();
				}
			});
			thread = null;
		}
	}
}
