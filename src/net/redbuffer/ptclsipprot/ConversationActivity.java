package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import org.linphone.ChatStorage;
import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatMessage.State;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ConversationActivity extends Activity implements LinphoneChatMessage.StateListener {
	public static int conversationId = 0;
	public static String username = "";

	public String domain = "";
	public boolean goToChat = false;
	
	private static ConversationActivity instance;
	private DatabaseHandler db;
	
	private Button sendButton = null;
	private TextView messageTextView = null;
	
	private MessageManager msgManager;
	
	private ConversationThreadListItemAdapter arrayAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conversation);
		
		instance = this;
		db = DatabaseHandler.getInstance(getBaseContext());
		
		Intent intent = getIntent();

		conversationId = intent.getIntExtra("ConversationActivity.conversationId", 0);
		username = intent.getStringExtra(username);
		
		LinphonePreferences prefs = LinphonePreferences.instance();
		domain = prefs.getAccountDomain(prefs.getDefaultAccountIndex());
		
		ChatStorage.getInstance().markConversationAsRead(LinphoneManager
				.getLcIfManagerNotDestroyedOrNull()
				.getOrCreateChatRoom("sip:" + username + "@" + domain));
		
		final ArrayList<DatabaseHandler.Message> MessagesList = db.getConversationMessages(conversationId);
		final ListView conversationThreadListView = (ListView) findViewById(R.id.conversation_thread_view);
		arrayAdapter = new ConversationThreadListItemAdapter(this, android.R.layout.simple_list_item_1, MessagesList);
		conversationThreadListView.setAdapter(arrayAdapter);
		
		msgManager = new MessageManager(this);
		
		sendButton = (Button) findViewById(R.id.send_button);
		messageTextView = (TextView) findViewById(R.id.message_input_box);
		
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String messageText = messageTextView.getText().toString();
				messageTextView.setText("");
				if (messageText != "") {
					int id = db.createNewMessage(conversationId, 1, messageText, (int)(System.currentTimeMillis()/1000), 1);
					msgManager.sendTextMessage("sip:" + username + "@" + domain, messageText);
					
					DatabaseHandler.Message msg = db.getMessage(id);
					arrayAdapter.add(msg);
				}
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conversation, menu);
		return true;
	}
	
	public static boolean isInstantiated() {
		if (instance == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public static ConversationActivity getInstance() {
		return instance;
	}
	
	@Override
	protected void onDestroy() {
		instance = null;
		super.onDestroy();
	}
	
	public void onMessageReceived(final int id, LinphoneAddress from, final LinphoneChatMessage message) {
		this.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				DatabaseHandler.Message msg = db.getMessage(id);
				arrayAdapter.add(msg);
			}
		});
	}
	
	@Override
	public synchronized void onLinphoneChatMessageStateChanged(LinphoneChatMessage msg, State state) {
		// TODO Auto-generated method stub
	}
}
