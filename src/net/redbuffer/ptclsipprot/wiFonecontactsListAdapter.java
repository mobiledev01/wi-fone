package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import net.redbuffer.ptclsipprot.DatabaseHandler.WiFoneContact;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class wiFonecontactsListAdapter extends ArrayAdapter<WiFoneContact>
{
	private ArrayList<WiFoneContact> objects;
	
	public wiFonecontactsListAdapter(Context context,
			int textViewResourceId, ArrayList<WiFoneContact> contacts)
	{
		super(context, textViewResourceId, contacts);
		this.objects = contacts;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		
		if(v == null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.contacts_list_adapter, null);
		}
		
		WiFoneContact contact = objects.get(position);
		
		if(contact!=null)
		{
			TextView contactNumber = (TextView) v.findViewById(R.id.ContactNumber);
			TextView contactName = (TextView) v.findViewById(R.id.ContactName);
			TextView contactId = (TextView) v.findViewById(R.id.ContactId);
			
			Log.i("PTCLSIP:", contact.firstName+" : "+contact.lastName+" : "+contact.domain+" : "+contact.username);
			contactId.setText(contact.firstName);
			contactName.setText(contact.firstName+" "+contact.lastName);
			contactNumber.setText(contact.username+"@"+contact.domain);
		}
		return v;
	}
}

