package net.redbuffer.ptclsipprot;

import org.linphone.LinphoneSimpleListener.LinphoneOnCallStateChangedListener;
import org.linphone.LinphoneSimpleListener.LinphoneOnMessageReceivedListener;
import org.linphone.LinphoneSimpleListener.LinphoneOnRegistrationStateChangedListener;
import org.linphone.StatusFragment;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneFriend;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class PtclActivity extends FragmentActivity implements
OnClickListener, LinphoneOnCallStateChangedListener,
LinphoneOnMessageReceivedListener,
LinphoneOnRegistrationStateChangedListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ptcl);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ptcl, menu);
		return true;
	}

	@Override
	public void onRegistrationStateChanged(RegistrationState state) { }

	@Override
	public void onMessageReceived(LinphoneAddress from,
			LinphoneChatMessage message, int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCallStateChanged(LinphoneCall call, State state,
			String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	public static boolean isInstanciated() {
		// TODO Auto-generated method stub
		return false;
	}

	public static PtclActivity instance() {
		// TODO Auto-generated method stub
		return null;
	}

	public void displayCustomToast(String string, int lengthLong) {
		// TODO Auto-generated method stub
		
	}

	public void onNewSubscriptionRequestReceived(LinphoneFriend lf, String url) {
		// TODO Auto-generated method stub
		
	}

	public void onNotifyPresenceReceived(LinphoneFriend lf) {
		// TODO Auto-generated method stub
		
	}

	public void exit() {
		// TODO Auto-generated method stub
		
	}

	public void updateStatusFragment(StatusFragment statusFragment) {
		// TODO Auto-generated method stub
		
	}

	public StatusFragment getStatusFragment() {
		// TODO Auto-generated method stub
		return new StatusFragment();
	}
}
