package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import net.redbuffer.ptclsipprot.DatabaseHandler.CallLogItem;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class logFragment extends Fragment {

	ListView callLogListView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.log_fragment, container, false);
		try
		{
			final DatabaseHandler db = DatabaseHandler.getInstance(getActivity());
			final ArrayList<CallLogItem> callLog = db.getCallLog();
			if(callLog!=null)
			{
				
				callLogListView = (ListView) view.findViewById(R.id.callLogListView);
				CallLogAdapter arrayAdapter = new CallLogAdapter(getActivity(), android.R.layout.simple_list_item_1, callLog);
				callLogListView.setAdapter(arrayAdapter);
		
				callLogListView.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
						CallLogItem log = callLog.get(position);
						Log.i("PTCL-Log", log.number);
						Intent intent = new Intent(getActivity().getBaseContext(), CallingActivity.class);
						if (log.number != "") {
							String sipAddress = "";
							intent.putExtra(sipAddress, log.number);
							intent.putExtra("CallingActivity.configId", log.configId);
							startActivity(intent);
						} else {
							Toast.makeText(getActivity(), "Invalid phone number.", Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		}catch(Exception e)
		{
			Log.d("error in Log Fragments",e.toString());
			exceptionHandler(e.toString());
		}
			return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try
		{
			final DatabaseHandler db = DatabaseHandler.getInstance(getActivity());
			final ArrayList<CallLogItem> callLog = db.getCallLog();
			CallLogAdapter arrayAdapter = new CallLogAdapter(getActivity(), android.R.layout.simple_list_item_1, callLog);
			callLogListView.setAdapter(arrayAdapter);
			callLogListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					CallLogItem log = callLog.get(position);
					Log.i("PTCL-Log", log.number);
					Intent intent = new Intent(getActivity().getBaseContext(), CallingActivity.class);
					if (log.number != "") {
						String sipAddress = "";
						intent.putExtra(sipAddress, log.number);
						intent.putExtra("CallingActivity.configId", log.configId);
						startActivity(intent);
					} else {
						Toast.makeText(getActivity(), "Invalid phone number.", Toast.LENGTH_LONG).show();
					}
				}
			});
		}catch(Exception e)
		{
			Log.d("error in Log Fragments",e.toString());
			exceptionHandler(e.toString());
		}
	}
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(getActivity(), error, Toast.LENGTH_LONG);
		toast.show();
	}
	
}
