package net.redbuffer.ptclsipprot;

import org.linphone.LinphoneManager;
import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatMessage.State;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneCore;

import android.content.Context;

public class MessageManager implements LinphoneChatMessage.StateListener {
	
	DatabaseHandler db;
	
	public MessageManager(Context context) {
		db = DatabaseHandler.getInstance(context);
	}
	
	public LinphoneChatRoom getChatRoom(String sipUri) {
		LinphoneChatRoom chatRoom = null;
		if (LinphoneManager.isInstanciated()) {
			LinphoneCore lc = LinphoneManager.getLc();
			chatRoom = lc.getOrCreateChatRoom(sipUri);
		}
		
		return chatRoom;
	}
	
	public void destroyChatRoom(String sipUri) {
		LinphoneChatRoom chatRoom = LinphoneManager.getLc().getOrCreateChatRoom(sipUri);
		if (chatRoom != null) {
			chatRoom.destroy();
		}
	}
	
	public void sendTextMessage(String sipUri, String message) {
		LinphoneCore lc = LinphoneManager.getLcIfManagerNotDestroyedOrNull();
		LinphoneChatRoom chatRoom = lc.getOrCreateChatRoom(sipUri);
		boolean isNetworkReachable = lc == null ? false : lc.isNetworkReachable();
		
		if (chatRoom != null && message != null && isNetworkReachable) {
			LinphoneChatMessage chatMessage = chatRoom.createLinphoneChatMessage(message);
			chatRoom.sendMessage(chatMessage, this);
		} else if (!isNetworkReachable) {
			//LinphoneActivity.instance().displayCustomToast(getString(R.string.error_network_unreachable), Toast.LENGTH_LONG);
		}
	}
	
	public void onMessageReceived(final int id, LinphoneAddress from, final LinphoneChatMessage message) {
		// save the message into database and notify the listening classes
		/*Conversation conv = db.findExistingConversation(from.getUserName());
		int convId = 0;
		if (conv == null) {
			convId = db.createNewConversation(from.getUserName());
		} else {
			convId = conv.conversationId;
		}
		
		db.createNewMessage(convId, 0, message.getText(), (int)(System.currentTimeMillis()/1000), 0);
		
		// notify the listeners
		if (ConversationActivity.isInstantiated()) {
			ConversationActivity.getInstance().onMessageReceived(convId, from, message);
		}
		
		if (ConversationListActivity.isInstantiated()) {
			ConversationListActivity.getInstance().onMessageReceived(convId, from, message);
		}*/
	}
	
	@Override
	public synchronized void onLinphoneChatMessageStateChanged(LinphoneChatMessage msg, State state) {
		if (ConversationActivity.isInstantiated()) {
			ConversationActivity.getInstance().onLinphoneChatMessageStateChanged(msg, state);
		}
	}
}
