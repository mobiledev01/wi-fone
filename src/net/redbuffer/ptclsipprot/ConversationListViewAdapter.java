package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import net.redbuffer.ptclsipprot.DatabaseHandler.Message;

import org.linphone.ChatStorage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ConversationListViewAdapter extends ArrayAdapter<DatabaseHandler.Conversation>{
	private ArrayList<DatabaseHandler.Conversation> objects;
	private Context ctx;
	
	public ConversationListViewAdapter(Context context, int textViewResourceId, ArrayList<DatabaseHandler.Conversation> objects)
	{
		super(context, textViewResourceId, objects);
		this.objects = objects;
		this.ctx = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if(v==null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.conversation_listview_adapter, null);
		}
		
		DatabaseHandler.Conversation conversation = objects.get(position);
		
		if(conversation != null)
		{
			TextView display_name = (TextView) v.findViewById(R.id.conversation_display_name);
			TextView user_id = (TextView) v.findViewById(R.id.conversation_user_id);
			TextView unread_count = (TextView) v.findViewById(R.id.conversation_unread_count);
			
			DatabaseHandler db = DatabaseHandler.getInstance(ctx);
			
			int msgCount = ChatStorage.getInstance().getUnreadMessageCount();
			Message msg = db.getLatestMessage(conversation.conversationId);
			
			display_name.setText(conversation.username);
			unread_count.setText(msgCount + " new message(s)");
			user_id.setText(msg.message);
		}
		return v;
	}
}
