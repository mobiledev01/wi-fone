package net.redbuffer.ptclsipprot;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class ErrorLogActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_error_log);
		
		LinearLayout ll = (LinearLayout) findViewById(R.id.errorLogLayout);
		
		for (int i = 0; i < 5; i++) {
			TableRow tr = new TableRow(this);
			LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			tr.setLayoutParams(lp);
			TextView tw = new TextView(this);
			tw.setText("this is a test: " + i);
			tr.addView(tr);
			ll.addView(tr);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.error_log, menu);
		return true;
	}

}
