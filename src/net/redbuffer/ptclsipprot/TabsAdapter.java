package net.redbuffer.ptclsipprot;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsAdapter extends FragmentPagerAdapter {

	public TabsAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int arg0) {
		// TODO Auto-generated method stub
		switch (arg0) {
		case 0:
			// Dialer Fragment Activity
			return new dialerFragment();
		case 1:
			// Contacts Fragment Activity
			return new contactsFragment();
		case 2:
			// Call Log Fragments Activity
			return new logFragment();
		case 3:
			// wi-fone Contacts Fragments Activity
			return new wiFoneContactsFragment();
			
		}

		return null;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 4;
	}
	
	public CharSequence  getPageTitle(int i){
		switch(i){
		case 0:
			return "Dialer";
		case 1:
			return "Contacts";
		case 2:
			return "Call Log";
		case 3:
			return "Wi-fone Contacts";
		}
		return null;
	}

}
