package net.redbuffer.ptclsipprot;

import java.util.ArrayList;

import net.redbuffer.ptclsipprot.DatabaseHandler.WiFoneContact;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class wiFoneContactsFragment extends Fragment {

	ListView contactsListView;
	ArrayList <WiFoneContact> contacts;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.wifone_contact_fragment,container, false);
		try
		{
			DatabaseHandler db=DatabaseHandler.getInstance(getActivity());
			//db.createNewWiFoneContact("tahir", "mehmood", "tms", "1234555");
			
			final ArrayList <WiFoneContact> contacts = db.getAllWiFoneContacts();
			contactsListView = (ListView) view.findViewById(R.id.wifonecontactsListView);
			wiFonecontactsListAdapter arrayAdapter = new wiFonecontactsListAdapter(getActivity(), android.R.layout.simple_list_item_1, contacts);
			//ArrayAdapter<String> arrayAdapter =  new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, contactNames);
			contactsListView.setAdapter(arrayAdapter);
			contactsListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					
					WiFoneContact contact = contacts.get(position);
					Intent intent = new Intent(getActivity().getBaseContext(), Wi_FoneActivity.class);
					String firstName = contact.firstName.toString();
					String lastName = contact.lastName.toString();
					String userName = contact.username.toString();
					String domain = contact.domain.toString();
					int contactId = contact.wiFoneContactId;
	
					if (firstName != "") {
	
						intent.putExtra("firstName",firstName);
						intent.putExtra("lastName",lastName);
						intent.putExtra("userName",userName);
						intent.putExtra("domain",domain);
						intent.putExtra("contactId",contactId);
	
						startActivity(intent);
					} else {
						Toast.makeText(getActivity(), "Invalid phone number.", Toast.LENGTH_LONG).show();
					}
				
				}
			});
		} catch(Exception e) {
			Log.d("Error at wiFoneContact fragments",e.toString());
			exceptionHandler(e.toString());
		}
		
		return view;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try
		{
			Log.i("PTCL TEST SIP","RESUME CALLED");
			
			DatabaseHandler db=DatabaseHandler.getInstance(getActivity());
	
			final ArrayList <WiFoneContact> contacts = db.getAllWiFoneContacts();
			wiFonecontactsListAdapter arrayAdapter = new wiFonecontactsListAdapter(getActivity(), android.R.layout.simple_list_item_1, contacts);
			contactsListView.setAdapter(arrayAdapter);
			contactsListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
					
					WiFoneContact contact = contacts.get(position);
					Intent intent = new Intent(getActivity().getBaseContext(), Wi_FoneActivity.class);
					String firstName = contact.firstName.toString();
					String lastName = contact.lastName.toString();
					String userName = contact.username.toString();
					String domain = contact.domain.toString();
					int contactId = contact.wiFoneContactId;
	
					if (firstName != "") 
					{
	
						intent.putExtra("firstName",firstName);
						intent.putExtra("lastName",lastName);
						intent.putExtra("userName",userName);
						intent.putExtra("domain",domain);
						intent.putExtra("contactId",contactId);
	
						startActivity(intent);
					} else {
						Toast.makeText(getActivity(), "Invalid phone number.", Toast.LENGTH_LONG).show();
					}
				
				}
			});
		}
		catch(Exception e)
		{
			Log.d("Error at fragments",e.toString());
			exceptionHandler(e.toString());
		}
		
	}
	
	
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(getActivity(), error, Toast.LENGTH_LONG);
		toast.show();
	}
	
	
}
