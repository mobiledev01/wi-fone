package net.redbuffer.ptclsipprot;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Edit_WiFone_contact extends Activity {

	public String firstName = "";	
    public String lastName = "";	
    public String userName = "";	
    public String domain = "";	
    public int contactId ;	
    public Boolean isAdd = false;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit__wi_fone_contact);
		
		try
		{
			final DatabaseHandler db = DatabaseHandler.getInstance(getBaseContext());
			Bundle extras = getIntent().getExtras();
			isAdd =extras.getBoolean("isAdd");
			
			final EditText firstName_edit=(EditText)findViewById(R.id.wi_contact_fn_edit);
			final EditText lastName_edit=(EditText)findViewById(R.id.wi_contact_ln_edit);
			final EditText userName_edit=(EditText)findViewById(R.id.wi_contact_un_edit);
			final EditText domain_edit=(EditText)findViewById(R.id.wi_contact_domain_edit);
			
			if(!isAdd)
			{
				firstName = extras.getString("firstName");
				lastName = extras.getString("lastName");
				userName = extras.getString("userName");
				domain = extras.getString("domain");
				contactId = extras.getInt("contactId");
				
				firstName_edit.setText(firstName);
				lastName_edit.setText(lastName);
				userName_edit.setText(userName);
				domain_edit.setText(domain);
				
				Button update=(Button)findViewById(R.id.wi_contact_update);
				update.setVisibility(View.VISIBLE);
				update.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View v) {
		        		int result = db.updateWiFoneContact(contactId, firstName_edit.getText().toString(), lastName_edit.getText().toString(), userName_edit.getText().toString(), domain_edit.getText().toString());
		        		if (result > 0) {
		        			finish();
		        		} else {
		        			Toast.makeText(getBaseContext(), "Error in updating contact.", Toast.LENGTH_LONG).show();
		        		}
		            }
		       });
			} else {
				Button addBtn=(Button)findViewById(R.id.wi_contact_addBtn);
				addBtn.setVisibility(View.VISIBLE);
				Log.d("here","hrer");
				addBtn.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View v) {	            	
		            	firstName = firstName_edit.getText().toString();
		    			lastName = lastName_edit.getText().toString();
		    			userName = userName_edit.getText().toString();
		    			domain = domain_edit.getText().toString();
		    			
		    			int result = db.createNewWiFoneContact(firstName, lastName, userName, domain);
		            	if (result > 0) {
		            		finish();
		            	} else {
		            		Toast.makeText(getBaseContext(), "Error in adding contact.", Toast.LENGTH_LONG).show();
		            	}
		            }
		       });
			}
		}
		catch(Exception e)
		{
			Log.d("error in calling activity",e.toString());
			exceptionHandler(e.toString());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit__wi_fone_contact, menu);
		return true;
	}
	public void exceptionHandler(String error)
	{
		Toast toast = Toast.makeText(this, error, Toast.LENGTH_LONG);
		toast.show();
	}
}
