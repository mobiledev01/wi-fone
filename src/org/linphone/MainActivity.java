/*package org.linphone;

import net.redbuffer.ptclsipprot.R;

import org.linphone.core.LinphoneAddress;
import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCall.State;
import org.linphone.core.LinphoneCallStats;
import org.linphone.core.LinphoneChatMessage;
import org.linphone.core.LinphoneChatRoom;
import org.linphone.core.LinphoneContent;
import org.linphone.core.LinphoneCore;
import org.linphone.core.LinphoneCore.EcCalibratorStatus;
import org.linphone.core.LinphoneCore.GlobalState;
import org.linphone.core.LinphoneCore.RegistrationState;
import org.linphone.core.LinphoneCoreException;
import org.linphone.core.LinphoneCoreFactory;
import org.linphone.core.LinphoneCoreListener;
import org.linphone.core.LinphoneEvent;
import org.linphone.core.LinphoneFriend;
import org.linphone.core.LinphoneInfoMessage;
import org.linphone.core.LinphoneProxyConfig;
import org.linphone.core.PublishState;
import org.linphone.core.SubscriptionState;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	Button registerButton, makeCallButton;
	TextView registrationStatus, callStatus;
	EditText serverIpET, usernameET, passwordET, phoneNumberET;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		registerButton = (Button) findViewById(R.id.ptcl_register);
		makeCallButton = (Button) findViewById(R.id.ptcl_make_call);
		
		registrationStatus = (TextView) findViewById(R.id.ptcl_register_status);
		callStatus = (TextView) findViewById(R.id.ptcl_call_status);
		
		serverIpET = (EditText) findViewById(R.id.ptcl_server_ip);
		usernameET = (EditText) findViewById(R.id.ptcl_username);
		passwordET = (EditText) findViewById(R.id.ptcl_password);
		phoneNumberET = (EditText) findViewById(R.id.ptcl_phone_number);
	}

	public void RegisterWithServer(View view) {
		String username = usernameET.getText().toString();
		String domain = serverIpET.getText().toString();
		String password = passwordET.getText().toString();
		String sipAddress = "sip:" + username + "@" + domain;
		
		final LinphoneCoreFactory lcFactory = LinphoneCoreFactory.instance();

		LinphoneCore lc = null;
		
		try {
			// First instantiate the core Linphone object given only a listener.
			// The listener will react to events in Linphone core.
			lc = lcFactory.createLinphoneCore(regListener);
			
			// Parse identity
			LinphoneAddress address = lcFactory.createLinphoneAddress("sip:" + username + "@" + domain);
			username = address.getUserName();
			domain = address.getDomain();

			if (password != null) {
				// create authentication structure from identity and add to linphone
				lc.addAuthInfo(lcFactory.createAuthInfo(username, password, null, domain));
			}

			// create proxy config
			LinphoneProxyConfig proxyCfg = lcFactory.createProxyConfig(sipAddress, domain, null, true);
			proxyCfg.setExpires(2000);
			lc.addProxyConfig(proxyCfg); // add it to linphone
			lc.setDefaultProxyConfig(proxyCfg);


			
			// main loop for receiving notifications and doing background linphonecore work
			boolean running = true;
			while (running) {
				lc.iterate(); // first iterate initiates registration 
				sleep(50);
			}


			// Unregister
			lc.getDefaultProxyConfig().edit();
			lc.getDefaultProxyConfig().enableRegister(false);
			lc.getDefaultProxyConfig().done();
			while(lc.getDefaultProxyConfig().getState() != RegistrationState.RegistrationCleared) {
				lc.iterate();
				sleep(50);
			}

			// Then register again
			lc.getDefaultProxyConfig().edit();
			lc.getDefaultProxyConfig().enableRegister(true);
			lc.getDefaultProxyConfig().done();

			while(lc.getDefaultProxyConfig().getState() != RegistrationState.RegistrationOk
					&& lc.getDefaultProxyConfig().getState() != RegistrationState.RegistrationFailed) {
				lc.iterate();
				sleep(50);
			}

			// Automatic unregistration on exit
		} catch(LinphoneCoreException lce) {
			
		}
		finally {
			//write("Shutting down linphone...");
			// You need to destroy the LinphoneCore object when no longer used
			lc.destroy();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void sleep(int ms) {
		try {
			Thread.sleep(ms);
		} catch(InterruptedException ie) {
			return;
		}
	}
	
	private LinphoneCoreListener regListener = new LinphoneCoreListener() {
		@Override
		public void registrationState(LinphoneCore lc, LinphoneProxyConfig cfg, RegistrationState cstate, String smessage) {
			if (cstate == RegistrationState.RegistrationProgress) {
				registrationStatus.setText("Registering...");
			} else if (cstate == RegistrationState.RegistrationNone) {
				registrationStatus.setText("No registration made.");
			} else if (cstate == RegistrationState.RegistrationCleared) {
				registrationStatus.setText("Registration cleared.");
			} else if (cstate == RegistrationState.RegistrationFailed) {
				registrationStatus.setText("Registration failed.");
			} else if (cstate == RegistrationState.RegistrationOk) {
				registrationStatus.setText("Registered.");
			}
		}

		@Override
		public void authInfoRequested(LinphoneCore lc, String realm,
				String username) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void globalState(LinphoneCore lc, GlobalState state,
				String message) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callState(LinphoneCore lc, LinphoneCall call, State cstate,
				String message) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callStatsUpdated(LinphoneCore lc, LinphoneCall call,
				LinphoneCallStats stats) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callEncryptionChanged(LinphoneCore lc, LinphoneCall call,
				boolean encrypted, String authenticationToken) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void newSubscriptionRequest(LinphoneCore lc, LinphoneFriend lf,
				String url) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void notifyPresenceReceived(LinphoneCore lc, LinphoneFriend lf) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void textReceived(LinphoneCore lc, LinphoneChatRoom cr,
				LinphoneAddress from, String message) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void messageReceived(LinphoneCore lc, LinphoneChatRoom cr,
				LinphoneChatMessage message) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dtmfReceived(LinphoneCore lc, LinphoneCall call, int dtmf) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void ecCalibrationStatus(LinphoneCore lc,
				EcCalibratorStatus status, int delay_ms, Object data) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void notifyReceived(LinphoneCore lc, LinphoneCall call,
				LinphoneAddress from, byte[] event) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void transferState(LinphoneCore lc, LinphoneCall call,
				State new_call_state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void infoReceived(LinphoneCore lc, LinphoneCall call,
				LinphoneInfoMessage info) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void subscriptionStateChanged(LinphoneCore lc, LinphoneEvent ev,
				SubscriptionState state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void notifyReceived(LinphoneCore lc, LinphoneEvent ev,
				String eventName, LinphoneContent content) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void publishStateChanged(LinphoneCore lc, LinphoneEvent ev,
				PublishState state) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void show(LinphoneCore lc) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void displayStatus(LinphoneCore lc, String message) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void displayMessage(LinphoneCore lc, String message) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void displayWarning(LinphoneCore lc, String message) {
			// TODO Auto-generated method stub
			
		}
	};

}
*/